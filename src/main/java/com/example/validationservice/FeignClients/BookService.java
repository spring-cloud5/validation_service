package com.example.validationservice.FeignClients;

import com.example.validationservice.dtos.Post;
import com.example.validationservice.dtos.PostDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(value = "bookservice",fallback = BookService.BookServiceFallBack.class)
public interface BookService {

    @RequestMapping(value = "/storePost", method = RequestMethod.POST)
    Post storePost(@RequestBody PostDTO postDTO);

    @Component
    static class BookServiceFallBack implements BookService{

        @Override
        public Post storePost(PostDTO postDTO) {
            return null;
        }
    }
}
