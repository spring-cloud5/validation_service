package com.example.validationservice.service;


import com.example.validationservice.FeignClients.BookService;
import com.example.validationservice.dtos.PostDTO;
import com.example.validationservice.payload.PostErrorResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PostService {

    @Autowired
    private BookService bookService;

    public void storePost(PostDTO postDTO) {
        String note = postDTO.getNote();
        String  title = postDTO.getTitle();
        List<String> errors = new ArrayList<>();
        if(note == null || title == null  || postDTO.getUser_id() == null){
            errors.add("Data Errors");
        }
        if(note.length() >= 100){
            errors.add("The Note too large length");
        }

        if(errors.size() == 0){
            // store the post
            this.bookService.storePost(postDTO);
        }

    }



}