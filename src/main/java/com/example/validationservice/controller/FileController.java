package com.example.validationservice.controller;

import com.example.validationservice.dtos.PostDTO;
import com.example.validationservice.payload.PostErrorResponse;
import com.example.validationservice.service.PostService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@CrossOrigin("*")
public class FileController {


    @Autowired
    private PostService postService;

    //@PostMapping("/createPost")
    @RabbitListener(queues = "${rabbitmq.posts.queue}")
    public void createPost(PostDTO postDTO) {
        postService.storePost(postDTO);
    }

}
